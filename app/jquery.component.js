System.register(["angular2/core"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var JqueryComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            JqueryComponent = (function () {
                function JqueryComponent(_elRef) {
                    this._elRef = _elRef;
                }
                JqueryComponent.prototype.ngOnInit = function () {
                    jQuery('this._elRef.nativeElement').find('button').on('click', function () {
                        console.log("gdgd");
                    });
                };
                JqueryComponent = __decorate([
                    core_1.Component({
                        selector: "my-jquery",
                        template: "<button>Click me</button>"
                    }), 
                    __metadata('design:paramtypes', [core_1.ElementRef])
                ], JqueryComponent);
                return JqueryComponent;
            }());
            exports_1("JqueryComponent", JqueryComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImpxdWVyeS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFTQTtnQkFDSSx5QkFBb0IsTUFBbUI7b0JBQW5CLFdBQU0sR0FBTixNQUFNLENBQWE7Z0JBQUUsQ0FBQztnQkFFMUMsa0NBQVEsR0FBUjtvQkFDSSxNQUFNLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRTt3QkFDNUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDdkIsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQztnQkFaTDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSxXQUFXO3dCQUNyQixRQUFRLEVBQUUsMkJBQTJCO3FCQUN4QyxDQUFDOzttQ0FBQTtnQkFVRixzQkFBQztZQUFELENBUkEsQUFRQyxJQUFBO1lBUkQsNkNBUUMsQ0FBQSIsImZpbGUiOiJqcXVlcnkuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmIH0gZnJvbSBcImFuZ3VsYXIyL2NvcmVcIjtcclxuZGVjbGFyZSB2YXIgalF1ZXJ5IDogYW55O1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6IFwibXktanF1ZXJ5XCIsXHJcbiAgICB0ZW1wbGF0ZTogYDxidXR0b24+Q2xpY2sgbWU8L2J1dHRvbj5gXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgSnF1ZXJ5Q29tcG9uZW50IHtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2VsUmVmIDogRWxlbWVudFJlZil7fVxyXG5cclxuICAgIG5nT25Jbml0KCk6YW55e1xyXG4gICAgICAgIGpRdWVyeSgndGhpcy5fZWxSZWYubmF0aXZlRWxlbWVudCcpLmZpbmQoJ2J1dHRvbicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgIGNvbnNvbGUubG9nKFwiZ2RnZFwiKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufSJdfQ==
