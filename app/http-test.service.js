System.register(["angular2/core", "angular2/http", 'rxjs/add/operator/map'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1;
    var HTTPTestService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {}],
        execute: function() {
            HTTPTestService = (function () {
                function HTTPTestService(_http) {
                    this._http = _http;
                }
                HTTPTestService.prototype.getAllData = function (limit, offset, searchString) {
                    var params = 'limit=' + limit;
                    params = params + '&orderBy=title&offset=' + offset;
                    if (!searchString) {
                        return this._http.get('http://gateway.marvel.com/v1/public/comics?ts=1&apikey=e6ce925b69783c4d254dd674ef9d3d8c&hash=72d1bf92fb32fd8324cc7ef004ddc778', { search: params })
                            .map(function (res) { return res.json(); });
                    }
                    else {
                        var paramsSearch = 'limit=' + limit;
                        paramsSearch = paramsSearch + '&offset=' + offset + '&orderBy=title&titleStartsWith=' + searchString;
                        return this._http.get('http://gateway.marvel.com/v1/public/comics?ts=1&apikey=e6ce925b69783c4d254dd674ef9d3d8c&hash=72d1bf92fb32fd8324cc7ef004ddc778', { search: paramsSearch })
                            .map(function (res) { return res.json(); });
                    }
                };
                HTTPTestService.prototype.getChar = function (url) {
                    return this._http.get(url + '?ts=1&apikey=e6ce925b69783c4d254dd674ef9d3d8c&hash=72d1bf92fb32fd8324cc7ef004ddc778')
                        .map(function (res) { return res.json(); });
                };
                HTTPTestService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], HTTPTestService);
                return HTTPTestService;
            }());
            exports_1("HTTPTestService", HTTPTestService);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImh0dHAtdGVzdC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQU1BO2dCQUdJLHlCQUFvQixLQUFZO29CQUFaLFVBQUssR0FBTCxLQUFLLENBQU87Z0JBQUUsQ0FBQztnQkFFbkMsb0NBQVUsR0FBVixVQUFXLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWTtvQkFDbEMsSUFBSSxNQUFNLEdBQUcsUUFBUSxHQUFDLEtBQUssQ0FBQztvQkFDNUIsTUFBTSxHQUFHLE1BQU0sR0FBRyx3QkFBd0IsR0FBQyxNQUFNLENBQUM7b0JBRWxELEVBQUUsQ0FBQSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUEsQ0FBQzt3QkFDZCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsK0hBQStILEVBQ2pKLEVBQUMsTUFBTSxFQUFFLE1BQU0sRUFBQyxDQUFDOzZCQUNoQixHQUFHLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLENBQUMsSUFBSSxFQUFFLEVBQVYsQ0FBVSxDQUFDLENBQUM7b0JBQ2hDLENBQUM7b0JBQUEsSUFBSSxDQUFBLENBQUM7d0JBQ0YsSUFBSSxZQUFZLEdBQUcsUUFBUSxHQUFDLEtBQUssQ0FBQzt3QkFDbEMsWUFBWSxHQUFHLFlBQVksR0FBRyxVQUFVLEdBQUMsTUFBTSxHQUFHLGlDQUFpQyxHQUFDLFlBQVksQ0FBQzt3QkFFakcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLCtIQUErSCxFQUNqSixFQUFDLE1BQU0sRUFBRSxZQUFZLEVBQUMsQ0FBQzs2QkFDdEIsR0FBRyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLElBQUksRUFBRSxFQUFWLENBQVUsQ0FBQyxDQUFDO29CQUNoQyxDQUFDO2dCQUNMLENBQUM7Z0JBRUQsaUNBQU8sR0FBUCxVQUFRLEdBQUc7b0JBQ1AsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxxRkFBcUYsQ0FBQzt5QkFDN0csR0FBRyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLElBQUksRUFBRSxFQUFWLENBQVUsQ0FBQyxDQUFDO2dCQUVoQyxDQUFDO2dCQTVCTDtvQkFBQyxpQkFBVSxFQUFFOzttQ0FBQTtnQkEyQ2Isc0JBQUM7WUFBRCxDQTFDQSxBQTBDQyxJQUFBO1lBMUNELDZDQTBDQyxDQUFBIiwiZmlsZSI6Imh0dHAtdGVzdC5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJbmplY3RhYmxlfSBmcm9tIFwiYW5ndWxhcjIvY29yZVwiO1xyXG5pbXBvcnQge0h0dHB9IGZyb20gXCJhbmd1bGFyMi9odHRwXCI7XHJcbmltcG9ydCAncnhqcy9hZGQvb3BlcmF0b3IvbWFwJztcclxuaW1wb3J0IHtIZWFkZXJzfSBmcm9tIFwiYW5ndWxhcjIvaHR0cFwiXHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBIVFRQVGVzdFNlcnZpY2V7XHJcblxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2h0dHAgOiBIdHRwKXt9XHJcblxyXG4gICAgZ2V0QWxsRGF0YShsaW1pdCwgb2Zmc2V0LCBzZWFyY2hTdHJpbmcpe1xyXG4gICAgICAgIHZhciBwYXJhbXMgPSAnbGltaXQ9JytsaW1pdDtcclxuICAgICAgICBwYXJhbXMgPSBwYXJhbXMgKyAnJm9yZGVyQnk9dGl0bGUmb2Zmc2V0PScrb2Zmc2V0O1xyXG5cclxuICAgICAgICBpZighc2VhcmNoU3RyaW5nKXtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAuZ2V0KCdodHRwOi8vZ2F0ZXdheS5tYXJ2ZWwuY29tL3YxL3B1YmxpYy9jb21pY3M/dHM9MSZhcGlrZXk9ZTZjZTkyNWI2OTc4M2M0ZDI1NGRkNjc0ZWY5ZDNkOGMmaGFzaD03MmQxYmY5MmZiMzJmZDgzMjRjYzdlZjAwNGRkYzc3OCcsXHJcbiAgICAgICAgICAgICAgICB7c2VhcmNoOiBwYXJhbXN9KVxyXG4gICAgICAgICAgICAgICAgLm1hcChyZXMgPT4gcmVzLmpzb24oKSk7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIHZhciBwYXJhbXNTZWFyY2ggPSAnbGltaXQ9JytsaW1pdDtcclxuICAgICAgICAgICAgcGFyYW1zU2VhcmNoID0gcGFyYW1zU2VhcmNoICsgJyZvZmZzZXQ9JytvZmZzZXQgKyAnJm9yZGVyQnk9dGl0bGUmdGl0bGVTdGFydHNXaXRoPScrc2VhcmNoU3RyaW5nO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2h0dHAuZ2V0KCdodHRwOi8vZ2F0ZXdheS5tYXJ2ZWwuY29tL3YxL3B1YmxpYy9jb21pY3M/dHM9MSZhcGlrZXk9ZTZjZTkyNWI2OTc4M2M0ZDI1NGRkNjc0ZWY5ZDNkOGMmaGFzaD03MmQxYmY5MmZiMzJmZDgzMjRjYzdlZjAwNGRkYzc3OCcsXHJcbiAgICAgICAgICAgICAgICB7c2VhcmNoOiBwYXJhbXNTZWFyY2h9KVxyXG4gICAgICAgICAgICAgICAgLm1hcChyZXMgPT4gcmVzLmpzb24oKSk7ICAgICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldENoYXIodXJsKXtcclxuICAgICAgICByZXR1cm4gdGhpcy5faHR0cC5nZXQodXJsICsgJz90cz0xJmFwaWtleT1lNmNlOTI1YjY5NzgzYzRkMjU0ZGQ2NzRlZjlkM2Q4YyZoYXNoPTcyZDFiZjkyZmIzMmZkODMyNGNjN2VmMDA0ZGRjNzc4JylcclxuICAgICAgICAgICAgLm1hcChyZXMgPT4gcmVzLmpzb24oKSk7XHJcbiAgICAgICAgXHJcbiAgICB9ICAgIFxyXG5cclxuICAgIC8vIHBvc3RKU09OKCl7XHJcbiAgICAvLyAgICAgdmFyIGpzb24gPSBKU09OLnN0cmluZ2lmeSh7dmFyMSA6ICd0ZXN0JywgdmFyMjogM30pO1xyXG4gICAgLy8gICAgIHZhciBwYXJhbXMgPSAnanNvbj0nICsganNvbjtcclxuICAgIC8vICAgICB2YXIgaGVhZGVycyA9IG5ldyBIZWFkZXJzKCk7XHJcbiAgICAvLyAgICAgaGVhZGVycy5hcHBlbmQoJ0NvbnRlbnQtVHlwZScsICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnKTtcclxuXHJcbiAgICAvLyAgICAgcmV0dXJuIHRoaXMuX2h0dHAucG9zdCgnaHR0cDovL3ZhbGlkYXRlLmpzb250ZXN0LmNvbScsIFxyXG4gICAgLy8gICAgICAgICAgICAgcGFyYW1zLCB7XHJcbiAgICAvLyAgICAgICAgICAgICAgICAgaGVhZGVyczogaGVhZGVyc1xyXG4gICAgLy8gICAgICAgICAgICAgfSlcclxuICAgIC8vICAgICAgICAgICAgIC5tYXAocmVzID0+IHJlcy5qc29uKCkpO1xyXG4gICAgICAgIFxyXG4gICAgLy8gfVxyXG59Il19
