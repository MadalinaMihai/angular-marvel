System.register(['angular2/core', "./http-test.service"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_test_service_1;
    var HTTPTestComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_test_service_1_1) {
                http_test_service_1 = http_test_service_1_1;
            }],
        execute: function() {
            HTTPTestComponent = (function () {
                function HTTPTestComponent(_httpService) {
                    this._httpService = _httpService;
                    this.arr = new Array();
                    this.pArr = new Array();
                    this.lArr = new Array();
                    this.dArr = new Array();
                    this.pagesArr = new Array();
                    this.pricesArr = new Array();
                    this.colectionNameArr = new Array();
                    this.storiesArr = new Array();
                    this.cArr = new Array();
                    this.totalItems = 0;
                    this.limit = 10;
                    this.offset = 0;
                }
                HTTPTestComponent.prototype.showSomeTitles = function (rData) {
                    var i = 0;
                    var data;
                    var charData;
                    var charInfo;
                    var name;
                    var cover;
                    var language;
                    var descr;
                    var price;
                    var noChars;
                    var colName;
                    var pages;
                    var noStories;
                    rData = JSON.stringify(rData);
                    this.parseData = JSON.parse(rData);
                    data = this.parseData['data']['results'];
                    this.totalItems = this.parseData['data']['total'];
                    for (var propertyName in this.parseData['data']['results']) {
                        if (this.parseData['data']['results'][propertyName]['textObjects'].length > 0) {
                            language = this.parseData['data']['results'][propertyName]['textObjects'][0]['language'];
                        }
                        colName = this.parseData['data']['results'][propertyName]['collections'][0];
                        if (colName == null) {
                            colName = 'No details about collection available.';
                        }
                        else {
                            this.colectionNameArr.push(colName);
                        }
                        price = this.parseData['data']['results'][propertyName]['prices'][0]['price'];
                        if (price == 0) {
                            price = 'No price available.';
                        }
                        else {
                            price = price + "$";
                        }
                        descr = this.parseData['data']['results'][propertyName]['description'];
                        if (descr == null) {
                            descr = 'No description available.';
                        }
                        noChars = this.parseData['data']['results'][propertyName]['characters']['available'];
                        noStories = this.parseData['data']['results'][propertyName]['stories']['available'];
                        for (var z = 0; z < noStories; z++) {
                            this.storiesArr[i] = [];
                            this.storiesArr[i][z] = this.parseData['data']['results'][propertyName]['stories']['items'][z]['name'];
                        }
                        // for(var z = 0; z < noChars; z++){
                        //     var hisLink = this.parseData['data']['results'][propertyName]['characters']['items'][z]['resourceURI'];
                        //     this._httpService.getChar(hisLink)
                        //     .subscribe(
                        //         function(res) { 
                        //             console.log("Success Character Response");
                        //             charData = JSON.stringify(res);
                        //             charInfo = JSON.parse(charData);
                        //             if(charInfo.data.results[0].name){
                        //                 console.log(charInfo.data.results[0].name);
                        //             }else{
                        //                 console.log('not found');
                        //             }
                        //         },
                        //         function(error) { console.log("Error happened" + error)},
                        //         function() { 
                        //         }
                        //     );  
                        // }
                        pages = this.parseData['data']['results'][propertyName]['pageCount'];
                        name = this.parseData['data']['results'][propertyName]['title'];
                        cover = this.parseData['data']['results'][propertyName]['thumbnail']['path'] + '/portrait_uncanny' + '.' + this.parseData['data']['results'][propertyName]['thumbnail']['extension'];
                        this.arr.push(name);
                        this.pArr.push(cover);
                        this.lArr.push(language);
                        this.dArr.push(decodeURIComponent(descr));
                        this.pricesArr.push(decodeURIComponent(price));
                        this.pagesArr.push(pages);
                        i++;
                    }
                };
                HTTPTestComponent.prototype.onTestGet = function (limit, offset, searched) {
                    var _this = this;
                    var data;
                    if (!this.stringSet) {
                        this._httpService.getAllData(limit, offset, searched)
                            .subscribe(function (res) {
                            console.log("Success Response");
                            data = res;
                        }, function (error) { console.log("Error happened" + error); }, function () {
                        });
                    }
                    else {
                        this._httpService.getAllData(limit, offset, this.stringSet)
                            .subscribe(function (res) {
                            console.log("Success Response");
                            data = res;
                        }, function (error) { console.log("Error happened" + error); }, function () {
                        });
                    }
                    var divToChange = document.getElementById('loader');
                    divToChange.style.display = 'block';
                    var pagHide = document.getElementById('pagination');
                    pagHide.style.display = 'none';
                    setTimeout(function () { return divToChange.style.display = 'none'; }, 4999);
                    setTimeout(function () { return pagHide.style.display = 'block'; }, 5000);
                    setTimeout(function () { return _this.showSomeTitles(data); }, 5000);
                };
                HTTPTestComponent.prototype.clearItems = function () {
                    this.arr = [];
                    this.pArr = [];
                    this.dArr = [];
                    this.lArr = [];
                    this.pricesArr = [];
                };
                HTTPTestComponent.prototype.newResults = function (item) {
                    if (item == 0) {
                        this.offset = 0;
                        this.clearItems();
                        this.onTestGet(this.limit, this.offset, null);
                    }
                    else if (item == 1) {
                        if (this.offset > 10) {
                            this.offset = this.offset - 10;
                            this.clearItems();
                            this.onTestGet(this.limit, this.offset, null);
                        }
                        else {
                            console.error("This offset is already at the lowest point!");
                        }
                    }
                    else if (item == 2) {
                        var dif = this.totalItems - 10;
                        if (this.offset < dif) {
                            this.offset = this.offset + 10;
                            this.clearItems();
                            this.onTestGet(this.limit, this.offset, null);
                        }
                        else {
                            console.error("This offset is already at the max point!");
                        }
                    }
                    else if (item == 3) {
                        this.offset = this.totalItems - 10;
                        this.clearItems();
                        this.onTestGet(this.limit, this.offset, null);
                    }
                };
                HTTPTestComponent.prototype.loadSearch = function (searched, key) {
                    if (key == 13) {
                        this.arr = [];
                        this.pArr = [];
                        this.stringSet = searched;
                        this.onTestGet(this.limit, this.offset, searched);
                    }
                };
                HTTPTestComponent.prototype.ngOnInit = function () {
                    this.onTestGet(this.limit, this.offset, null);
                };
                HTTPTestComponent = __decorate([
                    core_1.Component({
                        selector: 'http-test',
                        templateUrl: '/dev/interface.html',
                        styleUrls: ['/dev/style.css'],
                        providers: [http_test_service_1.HTTPTestService]
                    }), 
                    __metadata('design:paramtypes', [http_test_service_1.HTTPTestService])
                ], HTTPTestComponent);
                return HTTPTestComponent;
            }());
            exports_1("HTTPTestComponent", HTTPTestComponent);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImh0dHAtdGVzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFXQTtnQkFnQkksMkJBQXFCLFlBQTZCO29CQUE3QixpQkFBWSxHQUFaLFlBQVksQ0FBaUI7b0JBZGxELFFBQUcsR0FBRyxJQUFJLEtBQUssRUFBRSxDQUFDO29CQUNsQixTQUFJLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztvQkFDbkIsU0FBSSxHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7b0JBQ25CLFNBQUksR0FBRyxJQUFJLEtBQUssRUFBRSxDQUFDO29CQUNuQixhQUFRLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztvQkFDdkIsY0FBUyxHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7b0JBQ3hCLHFCQUFnQixHQUFHLElBQUksS0FBSyxFQUFFLENBQUM7b0JBQy9CLGVBQVUsR0FBRyxJQUFJLEtBQUssRUFBRSxDQUFDO29CQUN6QixTQUFJLEdBQUcsSUFBSSxLQUFLLEVBQUUsQ0FBQztvQkFDbkIsZUFBVSxHQUFHLENBQUMsQ0FBQztvQkFDZixVQUFLLEdBQUcsRUFBRSxDQUFDO29CQUNYLFdBQU0sR0FBRyxDQUFDLENBQUM7Z0JBR3lDLENBQUM7Z0JBRXJELDBDQUFjLEdBQWQsVUFBZSxLQUFLO29CQUNoQixJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ1YsSUFBSSxJQUFJLENBQUM7b0JBQ1QsSUFBSSxRQUFRLENBQUM7b0JBQ2IsSUFBSSxRQUFRLENBQUM7b0JBQ2IsSUFBSSxJQUFJLENBQUM7b0JBQ1QsSUFBSSxLQUFLLENBQUM7b0JBQ1YsSUFBSSxRQUFRLENBQUM7b0JBQ2IsSUFBSSxLQUFLLENBQUM7b0JBQ1YsSUFBSSxLQUFLLENBQUM7b0JBQ1YsSUFBSSxPQUFPLENBQUM7b0JBQ1osSUFBSSxPQUFPLENBQUM7b0JBQ1osSUFBSSxLQUFLLENBQUM7b0JBQ1YsSUFBSSxTQUFTLENBQUM7b0JBRWQsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQzlCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDbkMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBRXpDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFFbEQsR0FBRyxDQUFDLENBQUMsSUFBSSxZQUFZLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3pELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFBLENBQUM7NEJBQzVFLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3dCQUMzRixDQUFDO3dCQUVELE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUU1RSxFQUFFLENBQUEsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLENBQUEsQ0FBQzs0QkFDaEIsT0FBTyxHQUFHLHdDQUF3QyxDQUFDO3dCQUN2RCxDQUFDO3dCQUFBLElBQUksQ0FBQSxDQUFDOzRCQUNILElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ3ZDLENBQUM7d0JBRUQsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBRTlFLEVBQUUsQ0FBQSxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQSxDQUFDOzRCQUNYLEtBQUssR0FBRyxxQkFBcUIsQ0FBQzt3QkFDbEMsQ0FBQzt3QkFBQSxJQUFJLENBQUEsQ0FBQzs0QkFDRixLQUFLLEdBQUcsS0FBSyxHQUFHLEdBQUcsQ0FBQzt3QkFDeEIsQ0FBQzt3QkFFRCxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQzt3QkFFdkUsRUFBRSxDQUFBLENBQUMsS0FBSyxJQUFJLElBQUssQ0FBQyxDQUFBLENBQUM7NEJBQ2YsS0FBSyxHQUFHLDJCQUEyQixDQUFDO3dCQUN4QyxDQUFDO3dCQUVELE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUNyRixTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFFcEYsR0FBRyxDQUFBLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQUMsQ0FBQzs0QkFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7NEJBQ3hCLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDM0csQ0FBQzt3QkFFRCxvQ0FBb0M7d0JBQ3BDLDhHQUE4Rzt3QkFFOUcseUNBQXlDO3dCQUN6QyxrQkFBa0I7d0JBQ2xCLDJCQUEyQjt3QkFDM0IseURBQXlEO3dCQUN6RCw4Q0FBOEM7d0JBQzlDLCtDQUErQzt3QkFFL0MsaURBQWlEO3dCQUNqRCw4REFBOEQ7d0JBQzlELHFCQUFxQjt3QkFDckIsNENBQTRDO3dCQUM1QyxnQkFBZ0I7d0JBRWhCLGFBQWE7d0JBQ2Isb0VBQW9FO3dCQUNwRSx3QkFBd0I7d0JBRXhCLFlBQVk7d0JBQ1osV0FBVzt3QkFDWCxJQUFJO3dCQUVKLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUNyRSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDaEUsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsbUJBQW1CLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUM7d0JBQ3JMLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDdEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQy9DLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUMxQixDQUFDLEVBQUUsQ0FBQztvQkFDUixDQUFDO2dCQUNMLENBQUM7Z0JBRUQscUNBQVMsR0FBVCxVQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsUUFBUTtvQkFBakMsaUJBcUNDO29CQXBDRyxJQUFJLElBQUksQ0FBQztvQkFFVCxFQUFFLENBQUEsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQSxDQUFDO3dCQUNoQixJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQzs2QkFDcEQsU0FBUyxDQUNOLFVBQVMsR0FBRzs0QkFDUixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7NEJBQ2hDLElBQUksR0FBRyxHQUFHLENBQUM7d0JBQ2YsQ0FBQyxFQUNELFVBQVMsS0FBSyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDLENBQUEsQ0FBQSxDQUFDLEVBQ3hEO3dCQUVBLENBQUMsQ0FDSixDQUFDO29CQUNOLENBQUM7b0JBQUEsSUFBSSxDQUFBLENBQUM7d0JBQ0YsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDOzZCQUMxRCxTQUFTLENBQ04sVUFBUyxHQUFHOzRCQUNSLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQzs0QkFDaEMsSUFBSSxHQUFHLEdBQUcsQ0FBQzt3QkFDZixDQUFDLEVBQ0QsVUFBUyxLQUFLLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsQ0FBQSxDQUFBLENBQUMsRUFDeEQ7d0JBRUEsQ0FBQyxDQUNKLENBQUM7b0JBQ04sQ0FBQztvQkFFRCxJQUFJLFdBQVcsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUNwRCxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7b0JBQ3BDLElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ3BELE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztvQkFFL0IsVUFBVSxDQUFDLGNBQU0sT0FBQSxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLEVBQWxDLENBQWtDLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQzNELFVBQVUsQ0FBQyxjQUFNLE9BQUEsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxFQUEvQixDQUErQixFQUFFLElBQUksQ0FBQyxDQUFDO29CQUN4RCxVQUFVLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQXpCLENBQXlCLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3RELENBQUM7Z0JBRUQsc0NBQVUsR0FBVjtvQkFDSSxJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztvQkFDZCxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztvQkFDZixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztvQkFDZixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztvQkFDZixJQUFJLENBQUMsU0FBUyxHQUFFLEVBQUUsQ0FBQztnQkFDdkIsQ0FBQztnQkFFRCxzQ0FBVSxHQUFWLFVBQVcsSUFBSTtvQkFDWCxFQUFFLENBQUEsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUEsQ0FBQzt3QkFDVixJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzt3QkFFaEIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO3dCQUVsQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFDbEQsQ0FBQztvQkFBQSxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7d0JBQ2hCLEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLENBQUEsQ0FBQzs0QkFDakIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQzs0QkFDL0IsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDOzRCQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFDbEQsQ0FBQzt3QkFBQSxJQUFJLENBQUEsQ0FBQzs0QkFDRixPQUFPLENBQUMsS0FBSyxDQUFDLDZDQUE2QyxDQUFDLENBQUM7d0JBQ2pFLENBQUM7b0JBQ0wsQ0FBQztvQkFBQSxJQUFJLENBQUMsRUFBRSxDQUFBLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUM7d0JBQ2hCLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO3dCQUUvQixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFBLENBQUM7NEJBQ2xCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7NEJBQy9CLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQzs0QkFDbEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBQ2xELENBQUM7d0JBQUEsSUFBSSxDQUFBLENBQUM7NEJBQ0YsT0FBTyxDQUFDLEtBQUssQ0FBQywwQ0FBMEMsQ0FBQyxDQUFDO3dCQUM5RCxDQUFDO29CQUNMLENBQUM7b0JBQUEsSUFBSSxDQUFDLEVBQUUsQ0FBQSxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQSxDQUFDO3dCQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO3dCQUNuQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7d0JBRWxCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUNsRCxDQUFDO2dCQUNMLENBQUM7Z0JBRUQsc0NBQVUsR0FBVixVQUFXLFFBQVEsRUFBRSxHQUFHO29CQUVwQixFQUFFLENBQUEsQ0FBQyxHQUFHLElBQUksRUFBRSxDQUFDLENBQUEsQ0FBQzt3QkFDVixJQUFJLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQzt3QkFDZCxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQzt3QkFDZixJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQzt3QkFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7b0JBQ3RELENBQUM7Z0JBQ0wsQ0FBQztnQkFFRCxvQ0FBUSxHQUFSO29CQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUNsRCxDQUFDO2dCQWxOTDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSxXQUFXO3dCQUNyQixXQUFXLEVBQUUscUJBQXFCO3dCQUNsQyxTQUFTLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQzt3QkFFN0IsU0FBUyxFQUFHLENBQUMsbUNBQWUsQ0FBQztxQkFDaEMsQ0FBQzs7cUNBQUE7Z0JBNk1GLHdCQUFDO1lBQUQsQ0E1TUEsQUE0TUMsSUFBQTtZQTVNRCxpREE0TUMsQ0FBQSIsImZpbGUiOiJodHRwLXRlc3QuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIE9uSW5pdH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XHJcbmltcG9ydCB7IEhUVFBUZXN0U2VydmljZSB9IGZyb20gXCIuL2h0dHAtdGVzdC5zZXJ2aWNlXCI7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2h0dHAtdGVzdCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy9kZXYvaW50ZXJmYWNlLmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy9kZXYvc3R5bGUuY3NzJ10sXHJcbiAgICBcclxuICAgIHByb3ZpZGVycyA6IFtIVFRQVGVzdFNlcnZpY2VdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBIVFRQVGVzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBwYXJzZURhdGE6IGFueTtcclxuICAgIGFyciA9IG5ldyBBcnJheSgpO1xyXG4gICAgcEFyciA9IG5ldyBBcnJheSgpO1xyXG4gICAgbEFyciA9IG5ldyBBcnJheSgpO1xyXG4gICAgZEFyciA9IG5ldyBBcnJheSgpO1xyXG4gICAgcGFnZXNBcnIgPSBuZXcgQXJyYXkoKTtcclxuICAgIHByaWNlc0FyciA9IG5ldyBBcnJheSgpO1xyXG4gICAgY29sZWN0aW9uTmFtZUFyciA9IG5ldyBBcnJheSgpO1xyXG4gICAgc3Rvcmllc0FyciA9IG5ldyBBcnJheSgpO1xyXG4gICAgY0FyciA9IG5ldyBBcnJheSgpO1xyXG4gICAgdG90YWxJdGVtcyA9IDA7XHJcbiAgICBsaW1pdCA9IDEwO1xyXG4gICAgb2Zmc2V0ID0gMDtcclxuICAgIHN0cmluZ1NldCA6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3RvciAocHJpdmF0ZSBfaHR0cFNlcnZpY2U6IEhUVFBUZXN0U2VydmljZSl7fVxyXG5cclxuICAgIHNob3dTb21lVGl0bGVzKHJEYXRhKXtcclxuICAgICAgICB2YXIgaSA9IDA7XHJcbiAgICAgICAgdmFyIGRhdGE7XHJcbiAgICAgICAgdmFyIGNoYXJEYXRhO1xyXG4gICAgICAgIHZhciBjaGFySW5mbztcclxuICAgICAgICB2YXIgbmFtZTtcclxuICAgICAgICB2YXIgY292ZXI7XHJcbiAgICAgICAgdmFyIGxhbmd1YWdlO1xyXG4gICAgICAgIHZhciBkZXNjcjtcclxuICAgICAgICB2YXIgcHJpY2U7XHJcbiAgICAgICAgdmFyIG5vQ2hhcnM7XHJcbiAgICAgICAgdmFyIGNvbE5hbWU7XHJcbiAgICAgICAgdmFyIHBhZ2VzO1xyXG4gICAgICAgIHZhciBub1N0b3JpZXM7XHJcblxyXG4gICAgICAgIHJEYXRhID0gSlNPTi5zdHJpbmdpZnkockRhdGEpO1xyXG4gICAgICAgIHRoaXMucGFyc2VEYXRhID0gSlNPTi5wYXJzZShyRGF0YSk7XHJcbiAgICAgICAgZGF0YSA9IHRoaXMucGFyc2VEYXRhWydkYXRhJ11bJ3Jlc3VsdHMnXTtcclxuXHJcbiAgICAgICAgdGhpcy50b3RhbEl0ZW1zID0gdGhpcy5wYXJzZURhdGFbJ2RhdGEnXVsndG90YWwnXTtcclxuXHJcbiAgICAgICAgZm9yICh2YXIgcHJvcGVydHlOYW1lIGluIHRoaXMucGFyc2VEYXRhWydkYXRhJ11bJ3Jlc3VsdHMnXSkge1xyXG4gICAgICAgICAgICBpZih0aGlzLnBhcnNlRGF0YVsnZGF0YSddWydyZXN1bHRzJ11bcHJvcGVydHlOYW1lXVsndGV4dE9iamVjdHMnXS5sZW5ndGggPiAwKXtcclxuICAgICAgICAgICAgICBsYW5ndWFnZSA9IHRoaXMucGFyc2VEYXRhWydkYXRhJ11bJ3Jlc3VsdHMnXVtwcm9wZXJ0eU5hbWVdWyd0ZXh0T2JqZWN0cyddWzBdWydsYW5ndWFnZSddO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb2xOYW1lID0gdGhpcy5wYXJzZURhdGFbJ2RhdGEnXVsncmVzdWx0cyddW3Byb3BlcnR5TmFtZV1bJ2NvbGxlY3Rpb25zJ11bMF07XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZihjb2xOYW1lID09IG51bGwpe1xyXG4gICAgICAgICAgICAgICAgY29sTmFtZSA9ICdObyBkZXRhaWxzIGFib3V0IGNvbGxlY3Rpb24gYXZhaWxhYmxlLic7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICB0aGlzLmNvbGVjdGlvbk5hbWVBcnIucHVzaChjb2xOYW1lKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcHJpY2UgPSB0aGlzLnBhcnNlRGF0YVsnZGF0YSddWydyZXN1bHRzJ11bcHJvcGVydHlOYW1lXVsncHJpY2VzJ11bMF1bJ3ByaWNlJ107XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBpZihwcmljZSA9PSAwKXtcclxuICAgICAgICAgICAgICAgIHByaWNlID0gJ05vIHByaWNlIGF2YWlsYWJsZS4nO1xyXG4gICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgIHByaWNlID0gcHJpY2UgKyBcIiRcIjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgZGVzY3IgPSB0aGlzLnBhcnNlRGF0YVsnZGF0YSddWydyZXN1bHRzJ11bcHJvcGVydHlOYW1lXVsnZGVzY3JpcHRpb24nXTtcclxuXHJcbiAgICAgICAgICAgIGlmKGRlc2NyID09IG51bGwgKXtcclxuICAgICAgICAgICAgICAgIGRlc2NyID0gJ05vIGRlc2NyaXB0aW9uIGF2YWlsYWJsZS4nO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBub0NoYXJzID0gdGhpcy5wYXJzZURhdGFbJ2RhdGEnXVsncmVzdWx0cyddW3Byb3BlcnR5TmFtZV1bJ2NoYXJhY3RlcnMnXVsnYXZhaWxhYmxlJ107XHJcbiAgICAgICAgICAgIG5vU3RvcmllcyA9IHRoaXMucGFyc2VEYXRhWydkYXRhJ11bJ3Jlc3VsdHMnXVtwcm9wZXJ0eU5hbWVdWydzdG9yaWVzJ11bJ2F2YWlsYWJsZSddO1xyXG5cclxuICAgICAgICAgICAgZm9yKHZhciB6ID0gMDsgeiA8IG5vU3RvcmllczsgeisrKXtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3Rvcmllc0FycltpXSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdG9yaWVzQXJyW2ldW3pdID0gdGhpcy5wYXJzZURhdGFbJ2RhdGEnXVsncmVzdWx0cyddW3Byb3BlcnR5TmFtZV1bJ3N0b3JpZXMnXVsnaXRlbXMnXVt6XVsnbmFtZSddO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyBmb3IodmFyIHogPSAwOyB6IDwgbm9DaGFyczsgeisrKXtcclxuICAgICAgICAgICAgLy8gICAgIHZhciBoaXNMaW5rID0gdGhpcy5wYXJzZURhdGFbJ2RhdGEnXVsncmVzdWx0cyddW3Byb3BlcnR5TmFtZV1bJ2NoYXJhY3RlcnMnXVsnaXRlbXMnXVt6XVsncmVzb3VyY2VVUkknXTtcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAvLyAgICAgdGhpcy5faHR0cFNlcnZpY2UuZ2V0Q2hhcihoaXNMaW5rKVxyXG4gICAgICAgICAgICAvLyAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgLy8gICAgICAgICBmdW5jdGlvbihyZXMpIHsgXHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiU3VjY2VzcyBDaGFyYWN0ZXIgUmVzcG9uc2VcIik7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIGNoYXJEYXRhID0gSlNPTi5zdHJpbmdpZnkocmVzKTtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgY2hhckluZm8gPSBKU09OLnBhcnNlKGNoYXJEYXRhKTtcclxuXHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIGlmKGNoYXJJbmZvLmRhdGEucmVzdWx0c1swXS5uYW1lKXtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGNoYXJJbmZvLmRhdGEucmVzdWx0c1swXS5uYW1lKTtcclxuICAgICAgICAgICAgLy8gICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnbm90IGZvdW5kJyk7XHJcbiAgICAgICAgICAgIC8vICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIC8vICAgICAgICAgfSxcclxuICAgICAgICAgICAgLy8gICAgICAgICBmdW5jdGlvbihlcnJvcikgeyBjb25zb2xlLmxvZyhcIkVycm9yIGhhcHBlbmVkXCIgKyBlcnJvcil9LFxyXG4gICAgICAgICAgICAvLyAgICAgICAgIGZ1bmN0aW9uKCkgeyBcclxuICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIC8vICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyAgICAgKTsgIFxyXG4gICAgICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgICAgICBwYWdlcyA9IHRoaXMucGFyc2VEYXRhWydkYXRhJ11bJ3Jlc3VsdHMnXVtwcm9wZXJ0eU5hbWVdWydwYWdlQ291bnQnXTtcclxuICAgICAgICAgICAgbmFtZSA9IHRoaXMucGFyc2VEYXRhWydkYXRhJ11bJ3Jlc3VsdHMnXVtwcm9wZXJ0eU5hbWVdWyd0aXRsZSddO1xyXG4gICAgICAgICAgICBjb3ZlciA9IHRoaXMucGFyc2VEYXRhWydkYXRhJ11bJ3Jlc3VsdHMnXVtwcm9wZXJ0eU5hbWVdWyd0aHVtYm5haWwnXVsncGF0aCddICsgJy9wb3J0cmFpdF91bmNhbm55JyArICcuJyArIHRoaXMucGFyc2VEYXRhWydkYXRhJ11bJ3Jlc3VsdHMnXVtwcm9wZXJ0eU5hbWVdWyd0aHVtYm5haWwnXVsnZXh0ZW5zaW9uJ107XHJcbiAgICAgICAgICAgIHRoaXMuYXJyLnB1c2gobmFtZSk7XHJcbiAgICAgICAgICAgIHRoaXMucEFyci5wdXNoKGNvdmVyKTtcclxuICAgICAgICAgICAgdGhpcy5sQXJyLnB1c2gobGFuZ3VhZ2UpO1xyXG4gICAgICAgICAgICB0aGlzLmRBcnIucHVzaChkZWNvZGVVUklDb21wb25lbnQoZGVzY3IpKTtcclxuICAgICAgICAgICAgdGhpcy5wcmljZXNBcnIucHVzaChkZWNvZGVVUklDb21wb25lbnQocHJpY2UpKTtcclxuICAgICAgICAgICAgdGhpcy5wYWdlc0Fyci5wdXNoKHBhZ2VzKTtcclxuICAgICAgICAgICAgaSsrO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIFxyXG4gICAgb25UZXN0R2V0KGxpbWl0LCBvZmZzZXQsIHNlYXJjaGVkKXtcclxuICAgICAgICB2YXIgZGF0YTtcclxuXHJcbiAgICAgICAgaWYoIXRoaXMuc3RyaW5nU2V0KXtcclxuICAgICAgICAgICAgdGhpcy5faHR0cFNlcnZpY2UuZ2V0QWxsRGF0YShsaW1pdCwgb2Zmc2V0LCBzZWFyY2hlZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uKHJlcykgeyBcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlN1Y2Nlc3MgUmVzcG9uc2VcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YSA9IHJlcztcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBmdW5jdGlvbihlcnJvcikgeyBjb25zb2xlLmxvZyhcIkVycm9yIGhhcHBlbmVkXCIgKyBlcnJvcil9LFxyXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24oKSB7IFxyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApOyAgXHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIHRoaXMuX2h0dHBTZXJ2aWNlLmdldEFsbERhdGEobGltaXQsIG9mZnNldCwgdGhpcy5zdHJpbmdTZXQpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICBmdW5jdGlvbihyZXMpIHsgXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJTdWNjZXNzIFJlc3BvbnNlXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGRhdGEgPSByZXM7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgZnVuY3Rpb24oZXJyb3IpIHsgY29uc29sZS5sb2coXCJFcnJvciBoYXBwZW5lZFwiICsgZXJyb3IpfSxcclxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uKCkgeyBcclxuICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTsgXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB2YXIgZGl2VG9DaGFuZ2UgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnbG9hZGVyJyk7XHJcbiAgICAgICAgZGl2VG9DaGFuZ2Uuc3R5bGUuZGlzcGxheSA9ICdibG9jayc7XHJcbiAgICAgICAgdmFyIHBhZ0hpZGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncGFnaW5hdGlvbicpO1xyXG4gICAgICAgIHBhZ0hpZGUuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuXHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiBkaXZUb0NoYW5nZS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnLCA0OTk5KTsgIFxyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4gcGFnSGlkZS5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJywgNTAwMCk7ICAgICAgXHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB0aGlzLnNob3dTb21lVGl0bGVzKGRhdGEpLCA1MDAwKTtcclxuICAgIH1cclxuXHJcbiAgICBjbGVhckl0ZW1zKCl7XHJcbiAgICAgICAgdGhpcy5hcnIgPSBbXTtcclxuICAgICAgICB0aGlzLnBBcnIgPSBbXTtcclxuICAgICAgICB0aGlzLmRBcnIgPSBbXTtcclxuICAgICAgICB0aGlzLmxBcnIgPSBbXTtcclxuICAgICAgICB0aGlzLnByaWNlc0FyciA9W107XHJcbiAgICB9XHJcblxyXG4gICAgbmV3UmVzdWx0cyhpdGVtKXtcclxuICAgICAgICBpZihpdGVtID09IDApe1xyXG4gICAgICAgICAgICB0aGlzLm9mZnNldCA9IDA7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0aGlzLmNsZWFySXRlbXMoKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMub25UZXN0R2V0KHRoaXMubGltaXQsIHRoaXMub2Zmc2V0LCBudWxsKTtcclxuICAgICAgICB9ZWxzZSBpZihpdGVtID09IDEpe1xyXG4gICAgICAgICAgICBpZih0aGlzLm9mZnNldCA+IDEwKXtcclxuICAgICAgICAgICAgICAgIHRoaXMub2Zmc2V0ID0gdGhpcy5vZmZzZXQgLSAxMDtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2xlYXJJdGVtcygpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vblRlc3RHZXQodGhpcy5saW1pdCwgdGhpcy5vZmZzZXQsIG51bGwpO1xyXG4gICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJUaGlzIG9mZnNldCBpcyBhbHJlYWR5IGF0IHRoZSBsb3dlc3QgcG9pbnQhXCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfWVsc2UgaWYoaXRlbSA9PSAyKXtcclxuICAgICAgICAgICAgdmFyIGRpZiA9IHRoaXMudG90YWxJdGVtcyAtIDEwO1xyXG5cclxuICAgICAgICAgICAgaWYodGhpcy5vZmZzZXQgPCBkaWYpe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vZmZzZXQgPSB0aGlzLm9mZnNldCArIDEwO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jbGVhckl0ZW1zKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uVGVzdEdldCh0aGlzLmxpbWl0LCB0aGlzLm9mZnNldCwgbnVsbCk7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIlRoaXMgb2Zmc2V0IGlzIGFscmVhZHkgYXQgdGhlIG1heCBwb2ludCFcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9ZWxzZSBpZihpdGVtID09IDMpe1xyXG4gICAgICAgICAgICB0aGlzLm9mZnNldCA9IHRoaXMudG90YWxJdGVtcyAtIDEwO1xyXG4gICAgICAgICAgICB0aGlzLmNsZWFySXRlbXMoKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMub25UZXN0R2V0KHRoaXMubGltaXQsIHRoaXMub2Zmc2V0LCBudWxsKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbG9hZFNlYXJjaChzZWFyY2hlZCwga2V5KXtcclxuICAgICAgICBcclxuICAgICAgICBpZihrZXkgPT0gMTMpe1xyXG4gICAgICAgICAgICB0aGlzLmFyciA9IFtdO1xyXG4gICAgICAgICAgICB0aGlzLnBBcnIgPSBbXTtcclxuICAgICAgICAgICAgdGhpcy5zdHJpbmdTZXQgPSBzZWFyY2hlZDtcclxuICAgICAgICAgICAgdGhpcy5vblRlc3RHZXQodGhpcy5saW1pdCwgdGhpcy5vZmZzZXQsIHNlYXJjaGVkKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKXtcclxuICAgICAgICB0aGlzLm9uVGVzdEdldCh0aGlzLmxpbWl0LCB0aGlzLm9mZnNldCwgbnVsbCk7XHJcbiAgICB9XHJcbn0iXX0=
