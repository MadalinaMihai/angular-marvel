System.register(['angular2/core', "./http-test.service", "./http-test.component"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_test_service_1, http_test_component_1;
    var modifyJSON;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_test_service_1_1) {
                http_test_service_1 = http_test_service_1_1;
            },
            function (http_test_component_1_1) {
                http_test_component_1 = http_test_component_1_1;
            }],
        execute: function() {
            modifyJSON = (function () {
                function modifyJSON() {
                }
                modifyJSON.prototype.showSomeTitles = function () {
                    var i = 0;
                    var t = new http_test_component_1.HTTPTestComponent(this._httpService);
                    this.getData = t.onTestGet();
                    this.parseData = JSON.parse(this.getData);
                    for (var propertyName in this.parseData['data']['results']) {
                        for (var prop in this.parseData['data']['results'][propertyName]) {
                            if (prop == 'title') {
                                this.finalResults[prop] = this.parseData['data']['results'][propertyName][prop];
                                console.log(this.parseData['data']['results'][propertyName][prop]);
                            }
                        }
                        i++;
                    }
                    console.log(this.finalResults);
                    this.results = this.parseData;
                    this.getData = this.results;
                };
                modifyJSON = __decorate([
                    core_1.Component({
                        selector: 'mod-JSON',
                        template: "\n    ",
                        providers: [http_test_service_1.HTTPTestService]
                    }), 
                    __metadata('design:paramtypes', [])
                ], modifyJSON);
                return modifyJSON;
            }());
            exports_1("modifyJSON", modifyJSON);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1vZGlmeUpTT04udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFVQTtnQkFBQTtnQkFxQ0EsQ0FBQztnQkE1Qk0sbUNBQWMsR0FBckI7b0JBRVEsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNWLElBQUksQ0FBQyxHQUFHLElBQUksdUNBQWlCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUVqRCxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFHN0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFFMUMsR0FBRyxDQUFDLENBQUMsSUFBSSxZQUFZLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ3pELEdBQUcsQ0FBQSxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQSxDQUFDOzRCQUM3RCxFQUFFLENBQUEsQ0FBQyxJQUFJLElBQUksT0FBTyxDQUFDLENBQUEsQ0FBQztnQ0FDaEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2dDQUNoRixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzs0QkFDdkUsQ0FBQzt3QkFHTCxDQUFDO3dCQUNELENBQUMsRUFBRSxDQUFDO29CQUNSLENBQUM7b0JBRUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBRS9CLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztvQkFFOUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO2dCQUNwQyxDQUFDO2dCQTFDRDtvQkFBQyxnQkFBUyxDQUFDO3dCQUNQLFFBQVEsRUFBRSxVQUFVO3dCQUNwQixRQUFRLEVBQUUsUUFDVDt3QkFDRCxTQUFTLEVBQUcsQ0FBQyxtQ0FBZSxDQUFDO3FCQUNoQyxDQUFDOzs4QkFBQTtnQkFzQ0YsaUJBQUM7WUFBRCxDQXJDQSxBQXFDQyxJQUFBO1lBckNELG1DQXFDQyxDQUFBIiwiZmlsZSI6Im1vZGlmeUpTT04uanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudH0gZnJvbSAnYW5ndWxhcjIvY29yZSc7XHJcbmltcG9ydCB7IEhUVFBUZXN0U2VydmljZSB9IGZyb20gXCIuL2h0dHAtdGVzdC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7SFRUUFRlc3RDb21wb25lbnR9IGZyb20gXCIuL2h0dHAtdGVzdC5jb21wb25lbnRcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtb2QtSlNPTicsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgYCxcclxuICAgIHByb3ZpZGVycyA6IFtIVFRQVGVzdFNlcnZpY2VdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBtb2RpZnlKU09OIHtcclxuICAgIGdldERhdGE6IGFueTtcclxuICAgIGNydWREYXRhOiBzdHJpbmc7XHJcbiAgICBwYXJzZURhdGE6IGFueTtcclxuICAgIHJlc3VsdHM6IHN0cmluZztcclxuICAgIGZpbmFsUmVzdWx0czogYW55W107XHJcbiAgICBfaHR0cFNlcnZpY2UgOiBIVFRQVGVzdFNlcnZpY2U7XHJcblxyXG5cclxucHVibGljIHNob3dTb21lVGl0bGVzKCl7XHJcbiAgICAgIFxyXG4gICAgICAgIHZhciBpID0gMDtcclxuICAgICAgICB2YXIgdCA9IG5ldyBIVFRQVGVzdENvbXBvbmVudCh0aGlzLl9odHRwU2VydmljZSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5nZXREYXRhID0gdC5vblRlc3RHZXQoKTtcclxuXHJcblxyXG4gICAgICAgIHRoaXMucGFyc2VEYXRhID0gSlNPTi5wYXJzZSh0aGlzLmdldERhdGEpO1xyXG5cclxuICAgICAgICBmb3IgKHZhciBwcm9wZXJ0eU5hbWUgaW4gdGhpcy5wYXJzZURhdGFbJ2RhdGEnXVsncmVzdWx0cyddKSB7XHJcbiAgICAgICAgICAgIGZvcih2YXIgcHJvcCBpbiB0aGlzLnBhcnNlRGF0YVsnZGF0YSddWydyZXN1bHRzJ11bcHJvcGVydHlOYW1lXSl7XHJcbiAgICAgICAgICAgICAgICBpZihwcm9wID09ICd0aXRsZScpe1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmluYWxSZXN1bHRzW3Byb3BdID0gdGhpcy5wYXJzZURhdGFbJ2RhdGEnXVsncmVzdWx0cyddW3Byb3BlcnR5TmFtZV1bcHJvcF07XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5wYXJzZURhdGFbJ2RhdGEnXVsncmVzdWx0cyddW3Byb3BlcnR5TmFtZV1bcHJvcF0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5maW5hbFJlc3VsdHNbaV1bcHJvcF0gPSB0aGlzLnBhcnNlRGF0YVsnZGF0YSddWydyZXN1bHRzJ11bcHJvcGVydHlOYW1lXVtwcm9wXTsgXHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpKys7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuZmluYWxSZXN1bHRzKTtcclxuXHJcbiAgICAgICAgdGhpcy5yZXN1bHRzID0gdGhpcy5wYXJzZURhdGE7XHJcblxyXG4gICAgICAgIHRoaXMuZ2V0RGF0YSA9IHRoaXMucmVzdWx0cztcclxufVxyXG59Il19
