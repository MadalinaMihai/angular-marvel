import {Component} from 'angular2/core';
import { HTTPTestComponent } from "./http-test.component";

@Component({
    selector: 'my-app',
    template: `
        <div class = "main">
            <http-test></http-test>
        </div>  
    
    `,
    directives:[HTTPTestComponent]
})
export class AppComponent    {
    
}