import {Injectable} from "angular2/core";
import {Http} from "angular2/http";
import 'rxjs/add/operator/map';
import {Headers} from "angular2/http"

@Injectable()
export class HTTPTestService{


    constructor(private _http : Http){}

    getAllData(limit, offset, searchString){
        var params = 'limit='+limit;
        params = params + '&orderBy=title&offset='+offset;

        if(!searchString){
            return this._http.get('http://gateway.marvel.com/v1/public/comics?ts=1&apikey=e6ce925b69783c4d254dd674ef9d3d8c&hash=72d1bf92fb32fd8324cc7ef004ddc778',
                {search: params})
                .map(res => res.json());
        }else{
            var paramsSearch = 'limit='+limit;
            paramsSearch = paramsSearch + '&offset='+offset + '&orderBy=title&titleStartsWith='+searchString;

            return this._http.get('http://gateway.marvel.com/v1/public/comics?ts=1&apikey=e6ce925b69783c4d254dd674ef9d3d8c&hash=72d1bf92fb32fd8324cc7ef004ddc778',
                {search: paramsSearch})
                .map(res => res.json());            
        }
    }

    getChar(url){
        return this._http.get(url + '?ts=1&apikey=e6ce925b69783c4d254dd674ef9d3d8c&hash=72d1bf92fb32fd8324cc7ef004ddc778')
            .map(res => res.json());
        
    }    

    // postJSON(){
    //     var json = JSON.stringify({var1 : 'test', var2: 3});
    //     var params = 'json=' + json;
    //     var headers = new Headers();
    //     headers.append('Content-Type', 'application/x-www-form-urlencoded');

    //     return this._http.post('http://validate.jsontest.com', 
    //             params, {
    //                 headers: headers
    //             })
    //             .map(res => res.json());
        
    // }
}