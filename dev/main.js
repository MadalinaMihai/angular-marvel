var $play;
var $detail;
var $comic;
var $close;

var set = 0;

function init(){
  $play = $('.play'),
  $detail  = $('.detail'),
  $comic = $('.comic', $detail),
  $close = $('.close');
  
}

function close(){
  $p = $('.detail .poster');
  $p.css({
    top: $p.data('top'),
    left: $p.data('left'),
    width: $p.data('width'),
    height: $p.data('height'),
  })
  $('.main-stories').css('display', 'none');
  $detail.removeClass('ready').delay(500).queue(function(next){
    $(this).hide();
    $poster.removeClass('active');
    next();
  });
}

$('body').click(function(e){
  if(set == 0){
    set = 1;
    init();
  }

  $p = $(e.target).parents();
  if ($p.is('.app')){
    $('.comics .comic').click(function(){
      $comic.html($(this).html());

      $poster = $('.poster', this).addClass('active');
      
      $('.poster', $detail).css({
        top: $poster.position().top,
        left: $poster.position().left,
        width: $poster.width(),
        height: $poster.height()
      }).data({
        top: $poster.position().top,
        left: $poster.position().left,
        width: $poster.width(),
        height: $poster.height()
      })

      $detail.show();

      $('.poster', $detail).delay(10).queue(function(next) {
        $detail.addClass('ready');
        $('.main-stories').css('display', 'block');

        next();
      }).delay(100).queue(function(next){
        $(this).css({
          top: '-1%',
          left: '-2%',
          width: 374,
          height: 581
        });
        next();
      })
    })
  } else {
    close();
  }
})

