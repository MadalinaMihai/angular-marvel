import {Component, OnInit} from 'angular2/core';
import { HTTPTestService } from "./http-test.service";


@Component({
    selector: 'http-test',
    templateUrl: '/dev/interface.html',
    styleUrls: ['/dev/style.css'],
    
    providers : [HTTPTestService]
})
export class HTTPTestComponent implements OnInit {
    parseData: any;
    arr = new Array();
    pArr = new Array();
    lArr = new Array();
    dArr = new Array();
    pagesArr = new Array();
    pricesArr = new Array();
    colectionNameArr = new Array();
    storiesArr = new Array();
    cArr = new Array();
    totalItems = 0;
    limit = 10;
    offset = 0;
    stringSet : string;

    constructor (private _httpService: HTTPTestService){}

    showSomeTitles(rData){
        var i = 0;
        var data;
        var charData;
        var charInfo;
        var name;
        var cover;
        var language;
        var descr;
        var price;
        var noChars;
        var colName;
        var pages;
        var noStories;

        rData = JSON.stringify(rData);
        this.parseData = JSON.parse(rData);
        data = this.parseData['data']['results'];

        this.totalItems = this.parseData['data']['total'];

        for (var propertyName in this.parseData['data']['results']) {
            if(this.parseData['data']['results'][propertyName]['textObjects'].length > 0){
              language = this.parseData['data']['results'][propertyName]['textObjects'][0]['language'];
            }

            colName = this.parseData['data']['results'][propertyName]['collections'][0];
            
            if(colName == null){
                colName = 'No details about collection available.';
            }else{
               this.colectionNameArr.push(colName);
            }

            price = this.parseData['data']['results'][propertyName]['prices'][0]['price'];
            
            if(price == 0){
                price = 'No price available.';
            }else{
                price = price + "$";
            }

            descr = this.parseData['data']['results'][propertyName]['description'];

            if(descr == null ){
                descr = 'No description available.';
            }

            noChars = this.parseData['data']['results'][propertyName]['characters']['available'];
            noStories = this.parseData['data']['results'][propertyName]['stories']['available'];

            for(var z = 0; z < noStories; z++){
                this.storiesArr[i] = [];
                this.storiesArr[i][z] = this.parseData['data']['results'][propertyName]['stories']['items'][z]['name'];
            }

            // for(var z = 0; z < noChars; z++){
            //     var hisLink = this.parseData['data']['results'][propertyName]['characters']['items'][z]['resourceURI'];
                
            //     this._httpService.getChar(hisLink)
            //     .subscribe(
            //         function(res) { 
            //             console.log("Success Character Response");
            //             charData = JSON.stringify(res);
            //             charInfo = JSON.parse(charData);

            //             if(charInfo.data.results[0].name){
            //                 console.log(charInfo.data.results[0].name);
            //             }else{
            //                 console.log('not found');
            //             }
                        
            //         },
            //         function(error) { console.log("Error happened" + error)},
            //         function() { 
                        
            //         }
            //     );  
            // }

            pages = this.parseData['data']['results'][propertyName]['pageCount'];
            name = this.parseData['data']['results'][propertyName]['title'];
            cover = this.parseData['data']['results'][propertyName]['thumbnail']['path'] + '/portrait_uncanny' + '.' + this.parseData['data']['results'][propertyName]['thumbnail']['extension'];
            this.arr.push(name);
            this.pArr.push(cover);
            this.lArr.push(language);
            this.dArr.push(decodeURIComponent(descr));
            this.pricesArr.push(decodeURIComponent(price));
            this.pagesArr.push(pages);
            i++;
        }
    }
    
    onTestGet(limit, offset, searched){
        var data;

        if(!this.stringSet){
            this._httpService.getAllData(limit, offset, searched)
            .subscribe(
                function(res) { 
                    console.log("Success Response");
                    data = res;
                },
                function(error) { console.log("Error happened" + error)},
                function() { 
                    
                }
            );  
        }else{
            this._httpService.getAllData(limit, offset, this.stringSet)
            .subscribe(
                function(res) { 
                    console.log("Success Response");
                    data = res;
                },
                function(error) { console.log("Error happened" + error)},
                function() { 
                    
                }
            ); 
        }

        var divToChange = document.getElementById('loader');
        divToChange.style.display = 'block';
        var pagHide = document.getElementById('pagination');
        pagHide.style.display = 'none';

        setTimeout(() => divToChange.style.display = 'none', 4999);  
        setTimeout(() => pagHide.style.display = 'block', 5000);      
        setTimeout(() => this.showSomeTitles(data), 5000);
    }

    clearItems(){
        this.arr = [];
        this.pArr = [];
        this.dArr = [];
        this.lArr = [];
        this.pricesArr =[];
    }

    newResults(item){
        if(item == 0){
            this.offset = 0;
            
            this.clearItems();

            this.onTestGet(this.limit, this.offset, null);
        }else if(item == 1){
            if(this.offset > 10){
                this.offset = this.offset - 10;
                this.clearItems();
                this.onTestGet(this.limit, this.offset, null);
            }else{
                console.error("This offset is already at the lowest point!");
            }
        }else if(item == 2){
            var dif = this.totalItems - 10;

            if(this.offset < dif){
                this.offset = this.offset + 10;
                this.clearItems();
                this.onTestGet(this.limit, this.offset, null);
            }else{
                console.error("This offset is already at the max point!");
            }
        }else if(item == 3){
            this.offset = this.totalItems - 10;
            this.clearItems();

            this.onTestGet(this.limit, this.offset, null);
        }
    }

    loadSearch(searched, key){
        
        if(key == 13){
            this.arr = [];
            this.pArr = [];
            this.stringSet = searched;
            this.onTestGet(this.limit, this.offset, searched);
        }
    }

    ngOnInit(){
        this.onTestGet(this.limit, this.offset, null);
    }
}